import time

import main
import req_admxi


i = main.Sphere("SPY", 10000.0, 16.0)
i.update()
price = i.share.get_price()
i.exec_buy(15, price)
time.sleep(10)
print("cash:", req_admxi.get_cash("harmony"))
print("portfolio:", req_admxi.get_portfolio("harmony"))
print("orders:", req_admxi.get_orders("harmony"))
