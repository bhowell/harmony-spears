# Copyright 2015 Brendan Howell.
#
# This file is part of Harmony of the Spears.
#
#  Harmony of the Spears is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Harmony of the Spears is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Harmony of the Spears. If not, see <http://www.gnu.org/licenses/>.

import main

import yahoo_finance

s = main.Sphere("SPY", 10000.0, 16.0)
tape = yahoo_finance.Share("SPY")


# run for 1.5 years
for tick in tape.get_historical("2014-01-02", "2015-10-09"):
    price = float(tick["Close"])
    # fetch price
    s.push_price(price)

    if (s.indicator >= 1.0) and (s.cash >= 2500):
        shares = int(2500.0 / price)
        s.buy(shares, price)
        print("bought", shares, "at", price)
    if (s.indicator <= 0) and (s.shares > 0):
        shares = int(2500.0 / price)
        if s.shares < shares:
            shares = s.shares
        s.sell(shares, price)
        print("sold", shares, "at", price)

    s.update_nav(price)
    print("shares:", s.shares, "cash:", s.cash, "indicator:", s.indicator,
          "nav:", s.nav)
