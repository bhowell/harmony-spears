# Copyright 2015 Brendan Howell.
#
# This file is part of Harmony of the Spears.
#
#  Harmony of the Spears is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Harmony of the Spears is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Harmony of the Spears. If not, see <http://www.gnu.org/licenses/>.

import datetime
import glob
import json
import logging
import socket
import sqlite3
import time

import music21
import pytz
try:
    import req_admxi
except ImportError:
    logging.warning("Warning! req_admxi module not found.")
import yahoo_finance

logging.basicConfig(filename="harmony.log", level=logging.DEBUG)

MIDIRANGE = 87
CHROFFSET = 256
ALGO = "harmony"

with open("pass_key.txt", "r") as keyfile:
    pass_key = keyfile.read().strip()


def market_open():
    # returns True if NYSE is open (this does not work for holidays)
    ctime = datetime.datetime.now(pytz.timezone('US/Eastern'))
    chour = ctime.hour + (ctime.minute / 60.0)
    if (ctime.weekday() < 5) and (chour < 16) and (chour > 9.5):
        return True
    else:
        return False


def tochar(delta):
    # map int in range +/-44 to a unicode char for hash
    return chr(delta + 44 + 256)


def todelta(char):
    # map unicode character to a course delta
    return ord(char) - (44 + 256)


class Harmonies():
    # Harmony database from midi files
    # can match curves
    # two tables:
    #     files - a list of trained files
    #     strophes - musical curves with projected next note

    def __init__(self):
        self.conn = sqlite3.connect("data/db.sqlite")
        self.conn.row_factory = sqlite3.Row
        self.db = self.conn.cursor()

        # turn on case-sensitive LIKE queries
        self.db.execute("PRAGMA case_sensitive_like = 1")
        self.conn.commit()

        self.db.execute("select file from files")
        trained = []
        for song in self.db:
            trained.append(song[0])

        for mid in glob.glob("midi/*.mid"):
            if mid in trained:
                continue
            score = music21.converter.parse(mid)
            logging.debug(("training", mid))
            self.train(score, mid)
            self.db.execute("insert into files (file) values (?)", (mid, ))
        self.conn.commit()

    def now_playing(self, mid):
        with open("now_playing.txt", "w") as f:
            f.write(mid)

    def train(self, score, mid):
        for v in score.parts[0].voices:
            line = []
            for n in v.notes:
                if hasattr(n, "pitch"):
                    # print(n.pitch.midi)
                    line.append(n.pitch.midi)
                    # TODO: eventually do something with chords?
                    # TODO: eventually do something fun with n.lyric if not None
            self.saveStrophes(line, mid)

    def saveStrophes(self, line, mid):
        dtline = []
        for i in range(len(line) - 1):
            dtline.append(line[i + 1] - line[i])
        for j in range(len(dtline) - 5):
            istrophe = dtline[j:j + 4]
            next_dt = dtline[j + 5]
            strophe = ""
            for i in istrophe:
                strophe += tochar(i)
            self.db.execute("insert into strophes (strophe, next, midi) values (?,?,?)",
                            (strophe, next_dt, mid))

    def match(self, curve, dxrange=2.0):
        if len(curve) != 5:
            # maybe throw a real exception here?
            logging.warning("Error! Curve matching only works for 5 data points")
            return 0.0
        dtline = []
        for i in range(len(curve) - 1):
            dt_scaled = MIDIRANGE * ((curve[i + 1] - curve[i]) / dxrange)
            dtline.append(round(dt_scaled))

        strophe = ""
        for i in dtline:
            strophe += tochar(i)

        self.db.execute("select next, midi from strophes where strophe=?",
                        (strophe,))
        res = self.db.fetchone()

        if res:
            logging.debug(("got real match: ", res[0]))
            self.now_playing(res[1])
            return res[0]
        else:
            res = self.fuzzy_match(strophe)
            logging.debug(("curve:", curve, "fuzzy match", res[0][0],
                           "strophe:", res[1], "midi:", res[0]["midi"]))
            self.now_playing(res[0]["midi"])
            return int(res[0][0])

    def fuzzy_match(self, strophe):
        strophe = list(strophe)
        for i in range(4):
            tmp = strophe[:]
            tmp[i] = '_'
            self.db.execute(
                "select next, strophe, midi from strophes where strophe like ?",
                (''.join(tmp),))
            res = self.db.fetchall()
            if len(res) > 0:
                closest = self.find_closest(strophe, res)
                return closest, tmp

        for i in range(3):
            for j in range(i+1, 4):
                tmp = strophe[:]
                tmp[i] = '_'
                tmp[j] = '_'
                self.db.execute(
                    "select next, strophe, midi from strophes where strophe like ?",
                    (''.join(tmp), ))
                res = self.db.fetchall()
                if len(res) > 0:
                    closest = self.find_closest(strophe, res)
                    return closest, tmp

        for i in range(4):
            tmp = ['_', '_', '_', '_']
            tmp[i] = strophe[i]
            self.db.execute(
                "select next, strophe, midi from strophes where strophe like ?",
                (''.join(tmp),))
            res = self.db.fetchall()
            if len(res) > 0:
                closest = self.find_closest(strophe, res)
                return closest, tmp

        return [0, 0], "NOMATCH"

    def find_closest(self, strophe, matches):
        match = None
        match_dist = 5000

        for m in matches:
            dist = 0
            for i in range(len(strophe)):
                dist += abs(todelta(m["strophe"][i]) - todelta(strophe[i]))
            if dist < match_dist:
                match = m
                match_dist = dist
        logging.debug(("closest match to", strophe, "is", match["strophe"],
                       "distance", match_dist))
        return match


class Sphere():
    def __init__(self, ticker, value, dayrange):
        self.ticker = ticker
        self.share = yahoo_finance.Share(ticker)
        self.priceline = []  # last 6 quotes
        self.strophe = ""  # store the last 5 deltas as string
        self.indicator = 0.5  # buy/sell indicator
        self.nav = value
        self.cash = value
        self.shares = 0
        self.harmonies = Harmonies()
        self.dayrange = dayrange
        self.trade_serv = ("127.0.0.1", 8888)

    def buy(self, shares, price):
        # NOTE: this does not execute on the market! only updates internal acct
        self.cash -= shares * price
        self.shares += shares

    def sell(self, shares, price):
        # NOTE: does not execute on the market! only updates internal accounts
        self.cash += shares * price
        self.shares -= shares

    def push_price(self, price):
        self.priceline.append(price)
        if len(self.priceline) > 5:
            self.priceline = self.priceline[1:6]

        match = self.harmonies.match(self.priceline, self.dayrange)
        match = float(match) / MIDIRANGE

        self.indicator += match

        if self.indicator > 1:
            self.indicator = 1.0
        elif self.indicator < 0:
            self.indicator = 0.0

    def exec_trade(self, trade_json):
        server = socket.create_connection(self.trade_serv)
        server.settimeout(10)  # 10 seconds
        msg = trade_json.encode('utf-8')
        try:
            server.sendall(msg)
            resp = server.recv(1024)
            logging.debug(("reply:", resp))
        except socket.timeout:
            logging.debug("Order service timed out! Try again later.")
        server.close()

    def exec_buy(self, shares, price):
        # actually buy shares on the market.
        trade = {"pass_client": pass_key,
                 "contract": {"m_symbol": self.ticker,
                              "m_secType": "STK",
                              "m_exchange": "SMART",
                              "m_currency": "USD"
                              },
                 "order": {"m_action": "BUY",
                           "m_totalQuantity": shares,
                           "m_orderType": "LMT",
                           "m_lmtPrice": float(price),
                           "m_tif": "DAY",
                           "m_goodAfterTime": "",
                           "m_goodTillDate": ""
                           }
                 }
        trade_json = json.dumps(trade)
        logging.debug("buying", shares, "shares at", price)
        self.exec_trade(trade_json)

    def exec_sell(self, shares, price):
        # actually sell on the market
        trade = {"pass_client": pass_key,
                 "contract": {"m_symbol": self.ticker,
                              "m_secType": "STK",
                              "m_exchange": "SMART",
                              "m_currency": "USD"
                              },
                 "order": {"m_action": "SELL",
                           "m_totalQuantity": shares+1,  # test cheat
                           "m_orderType": "LMT",
                           "m_lmtPrice": float(price),
                           "m_tif": "DAY",
                           "m_goodAfterTime": "",
                           "m_goodTillDate": ""
                           }
                 }
        trade_json = json.dumps(trade)
        logging.debug("selling", shares, "shares at", price)
        self.exec_trade(trade_json)

    def update(self):
        self.cash = req_admxi.get_cash(ALGO)
        portfolio = req_admxi.get_portfolio(ALGO)
        # NOTE: this assumes we only have 1 issue in portfolio
        if len(portfolio) > 0:
            self.shares = int(portfolio[0][4])
        self.share.refresh()
        price = float(self.share.get_price())
        self.push_price(price)
        self.update_nav(price)

    def update_nav(self, price):
        self.nav = self.cash + (price * self.shares)


def main():
    i = Sphere("SPY", 10000.0, 16.0)
    while True:
        if market_open():
            logging.debug("markets are open...")
            i.update()
            logging.debug(("nav:", i.nav, "indicator:", i.indicator, "price:",
                  i.priceline[-1]))
            price = float(i.share.get_price())
            orders = req_admxi.get_orders(ALGO)  # only allow 1 order at a time
            if (i.indicator == 1.0) and (i.cash > 2500) and (len(orders) == 0):
                shares = int(2500 / price)
                i.exec_buy(shares, price)
            elif (i.indicator == 0) and (i.shares > 0) and (len(orders) == 0):
                shares = int(2500 / price)
                if i.shares < shares:
                    shares = i.shares
                i.exec_sell(shares, price)

        else:
            logging.debug("markets are closed. z z z z.....")

        time.sleep(900)  # 15 min

if __name__ == "__main__":
    main()
