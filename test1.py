from ib.ext.Contract import Contract
from ib.ext.Order import Order
from ib.opt import ibConnection

# connect to TWS
ibgw_conn = ibConnection(port=7496)
ibgw_conn.connect()

# create contract
my_contract = Contract()
my_contract.m_symbol = 'AAPL' # symbol for Apple
my_contract.m_secType = 'STK' # security type : on stock market
my_contract.m_exchange = 'SMART' # order manage by Interative Broker
my_contract.m_currency = 'USD' # currency used

# create order
my_order = Order()
my_order.m_action = 'BUY' # buy order
my_order.m_totalQuantity = 100 # quantity of stocks
my_order.m_orderType = 'LMT' # order type
my_order.m_lmtPrice = 111.89 # limit price
my_order.m_tif = 'DAY' # order available during the current day (TIF)
my_order.m_goodAfterTime = ''
my_order.m_goodTillDate = ''

# place order
order_id = 1
ibgw_conn.placeOrder(order_id, my_contract, my_order)
